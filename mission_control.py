#Daniel Brewer
#Paging Mission Control

#Import necessary packages
from datetime import datetime
import json
import sys

#Establish default variables
tele_data_tstat = {}
tele_data_batt = {}
json_output = []


#Function: Tele Operation Function
#Takes in a dictionary (tstat or batt), timestamp, satellite_id, and component name
#Will make adjustments to the given dictionary based on error requirements
def t_op(t_dic, ts, sat_id, comp):
    
    t_format = "%Y%m%d %H:%M:%S.%f"
    
    #Adds new array value if key is not present, updates value if it is
    if sat_id in t_dic:
        t_dic[sat_id].append(ts)
    else:
        t_dic[sat_id] = [ts]
        
    #Check if the third occurrence falls within 5 minutes of the first
    if len(t_dic[sat_id]) == 3:
        
        ts1 = datetime.strptime(t_dic[sat_id][0],t_format )
        ts2 = datetime.strptime(t_dic[sat_id][2],t_format)
        
        #Compute if within 5 minute threshold
        if (((ts2-ts1).seconds)/60) <= 5:
            
            ts1 = (ts1.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]) + "Z"
            
            if comp == "TSTAT":
                
                json_obj = json.dumps({'satelliteId': int(sat_id), \
                    'severity': "RED HIGH", 'component': comp, 'timestamp': ts1})
            
            else:
                
                json_obj = json.dumps({'satelliteId': int(sat_id),\
                    'severity': "RED LOW", 'component': comp, 'timestamp': ts1})
            
            json_output.append(json.loads(json_obj))             
                
            #Remove error array if error has been noted
            t_dic.pop(sat_id)
            
        else:
            
            #Remove oldest occurrence if not within 5 minute threshold
            t_dic[sat_id].pop(0)
    
    return t_dic

#Read file from command line argument
with open(sys.argv[1]) as file_point:
    
    line = file_point.readline()
    
    while line:
        
        #Parse lines into variables and clean new line chars
        ts, sat_id, rh_lim, yh_lim, yl_lim, rl_lim, raw_val, comp = line.split("|")
        comp = comp.rstrip()
        
        if comp == "TSTAT" and (float(raw_val) > float(rh_lim)):
            
            tele_data_tstat = t_op(tele_data_tstat, ts, sat_id, comp)
            
        elif comp == "BATT" and (float(raw_val) < float(rl_lim)):
            
            tele_data_batt = t_op(tele_data_batt, ts, sat_id, comp)
            
        line = file_point.readline()
        
print(json_output)